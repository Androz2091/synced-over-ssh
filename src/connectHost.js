import {
    PassThrough
} from 'node:stream';

import {
    chainedSession
} from '../lib/ssh.js';
import getHostChain from '../lib/getHostChain.js';

export default async ({
    hosts,
    host,
    activateScreen,
    setActiveStream
}) => {
    const
        {
            stream: serverStream,
            end: endClient
        } = await chainedSession({
            hostsChain: getHostChain(hosts, host)
        }),
        serverStdin = new PassThrough(),
        serverStdout = new PassThrough();
    serverStream.on('end', () => {
        endClient();
        activateScreen();
    });
    serverStream.on('data', data => {
        serverStdin.write(data);
        if(data.toString('utf8') === `[sudo] password for ${host.username}: `)
            serverStream.write(host.password);
    });
    setActiveStream({
        stdin: serverStdin,
        stdout: serverStdout,
        setSize: ({ rows, columns }) => serverStream.setWindow(rows, columns)
    });
    serverStdout.on('data', data => {
        switch(data.toString('hex')){
            case '1b3e': {
                serverStream.write(host.password);
                break;
            }
            default: {
                serverStream.write(data);
            }
        }
    });
};