import { getString } from '../../../strings.js';

import {
    form,
    text,
    textbox,
    flexContainer,
    button
} from '../components.js';

export default (
    screen
) => ({
    url,
    qr,
    callback
}) => {
    const otpSetupForm = form(screen);
    text(screen, { content: getString('enableTotp.title') }, { borderBottom: true });
    let n = 3;
    text(otpSetupForm, { top: n, content: `${getString('enableTotp.url')} : ${url}` });
    const qrText = text(otpSetupForm, { top: n + 2, content: `${getString('enableTotp.qrCode')} :\n${qr}` });
    text(otpSetupForm, { top: n += qrText.$height + 2, content: `${getString('enableTotp.code')} :` });
    const
        codeInput = textbox(otpSetupForm, { top: n += 2, name: 'code' }),
        actions = flexContainer(otpSetupForm, { top: n += 4 });
    button(actions, { content: getString('cancel') })
        .on('press', () => callback({ isCancelled: true }));
    button(actions, { content: getString('submit') }, { color: 'green' })
        .on('press', () => otpSetupForm.submit());
    const
        successText = text(otpSetupForm, { top: n += 4, content: getString('enableTotp.success'), hidden: true }, { color: 'green' }),
        errorText = text(otpSetupForm, { top: n, hidden: true }, { color: 'red' });
    otpSetupForm.on('submit', ({ code }) => callback({
        code,
        onError: message => {
            successText.hide();
            errorText.setContent(getString(message));
            errorText.show();
            switch(message){
                case 'invalidTotpCode': {
                    codeInput.clearValue();
                    otpSetupForm.focusChild(codeInput);
                    break;
                }
            }
        }
    }));
    screen.render();
    codeInput.focus();
};