import { getString } from '../../../strings.js';

import {
    form,
    flexContainer,
    button,
    table,
    text
} from '../components.js';

export default (
    screen
) => ({
    hosts,
    callback,
    isTotpEnabled,
    emitter
}) => {
    let n = 3;
    const
        mainActionsFormContainer = flexContainer(form(screen)),
        selectButton = button(mainActionsFormContainer, { content: getString('home.selectHost'), hidden: hosts.length === 0 }),
        addButton = button(mainActionsFormContainer, { content: getString('addHost') });
    addButton.on('press', () => callback({ action: 'add-host' }));
    button(mainActionsFormContainer, { content: getString(`home.${isTotpEnabled ? 'disableTotp' : 'enableTotp'}`) })
        .on('press', () => callback({ action: 'toggle-totp' }));
    button(mainActionsFormContainer, { content: getString('home.editUserPass') })
        .on('press', () => callback({ action: 'edit-user-pass' }));
    button(mainActionsFormContainer, { content: getString('home.deleteAccount') })
        .on('press', () => callback({ action: 'delete-account' }));
    button(mainActionsFormContainer, { content: getString('quit') })
        .on('press', () => callback({ action: 'quit' }));
    let selectedHost;
    const
        tableHeight = screen.height - 15,
        hostsTable = table(
            screen,
            {
                top: n += 1,
                width: 75,
                height: tableHeight,
                columnsWidth: [ 50, 50 ],
                headers: [ getString('home.host'), getString('home.uri') ],
                data: hosts.map(host => [
                    host.chain ? getString('home.chain', { host, via: host.chain.split(',').join(getString('home.chainSeparator')) }) : host.name,
                    getString('home.hostUri', { host })
                ])
            }
        );
    selectButton.on('press', () => {
        hostsTable.focus();
        emitter.on('escape', () => selectButton.focus());
    });
    const
        selectedHostForm = form(
            screen,
            {
                top: n + tableHeight + 1,
                width: 50,
                height: 9,
                padding: 1,
                hidden: true
            },
            { borders: true }
        ),
        selectedHostText = text(selectedHostForm),
        selectedHostActions = flexContainer(selectedHostForm, { top: 2, width: 75 }),
        cancelButton = button(selectedHostActions, { content: getString('cancel') }),
        connectButton = button(selectedHostActions, { content: getString('home.connect') }, { color: 'green' });
    button(selectedHostActions, { content: getString('home.edit') })
        .on('press', () => callback({ action: 'edit-host', host: selectedHost }));
    const removeButton = button(selectedHostActions, { content: getString('home.remove') }, { color: 'yellow' });
    cancelButton.on('press', () => {
        selectedHostForm.hide();
        screen.render();
        hostsTable.focus();
        removeButton.setContent(getString('home.remove'));
    });
    hostsTable.rows.on('select', (undefined, index) => {
        selectedHost = hosts[index];
        selectedHostText.setContent(`${getString('home.selectedHost')} : ${selectedHost.name}`);
        selectedHostForm.show();
        screen.render();
        cancelButton.focus();
    });
    connectButton.on('press', async () => {
        connectButton.setContent(getString('home.connecting'));
        screen.render();
        await callback({ action: 'connect-host', host: selectedHost });
        connectButton.setContent(getString('home.connect'));
    });
    removeButton.on('press', () => {
        if(removeButton.content === getString('home.remove')){
            removeButton.setContent(getString('confirm'));
            screen.render();
        }
        else if(removeButton.content === getString('confirm'))
            callback({ action: 'remove-host', host: selectedHost });
    });
    screen.render();
    if(hosts.length)
        selectButton.focus();
    else
        addButton.focus();
};