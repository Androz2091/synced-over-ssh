import { getString } from '../../../strings.js';

import {
    form,
    text,
    textbox,
    flexContainer,
    button
} from '../components.js';

export default (
    screen
) => ({
    username,
    isTotpEnabled,
    callback
}) => {
    const editUserPassForm = form(screen);
    text(editUserPassForm, { content: getString('editUserPass.title') }, { borderBottom: true });
    let n = 3;
    text(editUserPassForm, { top: n, content: `${getString('editUserPass.currentUsername')} : ${username}` });
    text(editUserPassForm, { top: n += 2, content: `${getString('editUserPass.newUsername')} :` });
    const newUsernameInput = textbox(editUserPassForm, { top: n += 2, name: 'newUsername' });
    text(editUserPassForm, { top: n += 4, content: `${getString('editUserPass.currentPassword')} :` });
    textbox(editUserPassForm, { top: n += 2, name: 'currentPassword', censor: true });
    text(editUserPassForm, { top: n += 4, content: `${getString('editUserPass.newPassword')} :` });
    textbox(editUserPassForm, { top: n += 2, name: 'newPassword', censor: true });
    text(editUserPassForm, { top: n += 4, content: `${getString('editUserPass.repeatNewPassword')} :` });
    textbox(editUserPassForm, { top: n += 2, name: 'repeatNewPassword', censor: true });
    if(isTotpEnabled){
        text(editUserPassForm, { top: n += 4, content: `${getString('totpCode')} :` });
        textbox(editUserPassForm, { top: n += 2, name: 'totpCode' });
    }
    const actions = flexContainer(editUserPassForm, { top: n += 4 });
    button(actions, { content: getString('cancel') })
        .on('press', () => callback({ isCancelled: true }));
    button(actions, { content: getString('submit') }, { color: 'green' })
        .on('press', () => editUserPassForm.submit());
    const errorText = text(
        editUserPassForm,
        { top: n + 4 },
        { color: 'red' }
    );
    editUserPassForm.on('submit', formData => callback({
        ...formData,
        onError: error => {
            errorText.setContent(getString(`editUserPass.${error}`, `$t(${error})`));
            screen.render();
        }
    }));
    screen.render();
    newUsernameInput.focus();
};