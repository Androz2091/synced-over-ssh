import { getString } from '../../../strings.js';

import {
    form,
    text,
    textbox,
    checkbox,
    button
} from '../components.js';

export default (
    screen
) => ({
    username,
    callback
}) => {
    const registrationForm = form(screen);
    text(registrationForm, { content: getString('register.title', { username }) });
    let n = 2;
    const repeatPasswordInput = textbox(registrationForm, { top: n, name: 'repeatPassword', censor: true });
    checkbox(registrationForm, { top: n += 4, name: 'enableTotp', content: getString('register.enableTotp') });
    const
        submitButton = button(registrationForm, { top: n += 2, content: getString('submit') }, { color: 'green' }),
        errorText = text(
            registrationForm,
            { top: n + 4, hidden: true },
            { color: 'red' }
        );
    registrationForm.on('submit', ({ repeatPassword, enableTotp }) => callback({
        repeatPassword,
        isEnableTotp: enableTotp,
        onError: message => {
            errorText.setContent(getString(`register.${message}`));
            errorText.show();
            switch(message){
                case 'passwordMismatch': {
                    repeatPasswordInput.clearValue();
                    registrationForm.focusChild(repeatPasswordInput);
                    break;
                }
            }
        }
    }));
    submitButton.on('press', () => registrationForm.submit());
    screen.render();
    repeatPasswordInput.focus();
};