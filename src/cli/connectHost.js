import chalk from 'chalk';

import {
    getString,
    getAnyString
} from '../../strings.js';

import connectHost from '../connectHost.js';

export default async ({
    setActiveStream,
    screenStream,
    user,
    name
}) => {
    screenStream.stdin.write(`${getString('connectHost.connecting')}\r\n`);
    const
        hosts = user.getHosts(),
        host = hosts.find(host => name === host.name);
    if(!host)
        return screenStream.stdin.write(`${chalk.red(getString('setHost.NOT_EXISTS'))}\n`);
    try {
        await connectHost({
            hosts,
            host,
            activateScreen: () => setActiveStream(screenStream),
            setActiveStream
        });
    }
    catch(error){
        setActiveStream(screenStream, false);
        screenStream.stdin.write(`${chalk.red(getAnyString(
            `setHost.${getString(`setHost.rawError.${error.level}`)}`,
            ['COMMAND_FAILED', { error }]
        ))}\n`);
    }
};