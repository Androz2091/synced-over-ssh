import { getString } from '../../../strings.js';

export default ({
    program,
    end
}) => program
    .command('quit')
    .aliases([
        'exit',
        'logout',
        'signout'
    ])
    .description(getString('quit'))
    .action(end);