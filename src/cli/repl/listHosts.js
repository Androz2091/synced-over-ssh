import { getString } from '../../../strings.js';

import listHosts from '../listHosts.js';

export default ({
    program,
    screenStream,
    user
}) => program
    .command('list-hosts')
    .alias('l')
    .description(getString('listHosts'))
    .action(() => listHosts({
        screenStream,
        user
    }));