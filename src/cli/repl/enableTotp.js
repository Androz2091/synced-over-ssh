import chalk from 'chalk';

import { getString } from '../../../strings.js';
import enableTotp from '../enableTotp.js';

export default ({
    program,
    screenStream,
    onAbort,
    user
}) => program
    .command('enable-totp')
    .description(getString('home.enableTotp'))
    .action(async () => {
        if(user.isTotpEnabled())
            return screenStream.stdin.write(`${chalk.red(getString('enableTotp.alreadyEnabledError'))}\n`);
        await enableTotp({
            screenStream,
            onAbort,
            user
        });
    });