import {
    getString
} from '../../../strings.js';

import connectHost from '../connectHost.js';

export default ({
    setActiveStream,
    program,
    screenStream,
    user
}) => program
    .command('connect-host')
    .alias('c')
    .description(getString('connectHost.title'))
    .argument('<name>', getString('setHost.name'))
    .action(async name => {
        await connectHost({
            setActiveStream,
            screenStream,
            user,
            name
        });
    });