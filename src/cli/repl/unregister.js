import inquirerPassword from '@inquirer/password';
import inquirerInput from '@inquirer/input';
import inquirerConfirm from '@inquirer/confirm';
import chalk from 'chalk';

import { getString } from '../../../strings.js';
import createInquirerContext from '../../../lib/createInquirerContext.js';

export default ({
    program,
    screenStream,
    onAbort,
    user,
    end
}) => program
    .command('unregister')
    .alias('delete-account')
    .description(getString('home.deleteAccount'))
    .action(async () => {
        let totpCode;
        const currentPassword = await inquirerPassword(
            {
                message: getString('editUserPass.currentPassword'),
                mask: '*'
            },
            createInquirerContext(screenStream, onAbort)
        );
        if(user.isTotpEnabled()){
            totpCode = await inquirerInput(
                {
                    message: getString('totpCode')
                },
                createInquirerContext(screenStream, onAbort)
            );
        }
        if(!await inquirerConfirm(
            {
                message: getString('confirm'),
                default: false
            },
            createInquirerContext(screenStream, onAbort)
        )){
            return screenStream.stdin.write(`${getString('deleteAccount.aborted')}\n`);
        }
        try {
            await user.unregister(
                currentPassword,
                totpCode
            );
            end();
        }
        catch(error){
            screenStream.stdin.write(`${chalk.red(getString(error.message))}\n`);
        }
    });