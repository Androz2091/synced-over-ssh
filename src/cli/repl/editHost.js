import {
    getString,
} from '../../../strings.js';

import editHost from '../editHost.js';

export default ({
    program,
    screenStream,
    onAbort,
    user
}) => program
    .command('edit-host')
    .description(getString('editHost'))
    .argument('[name]', getString('setHost.name'))
    .action(async oldName => {
        await editHost({
            screenStream,
            onAbort,
            user,
            oldName
        });
    });