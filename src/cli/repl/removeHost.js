import {
    getString
} from '../../../strings.js';

import removeHost from '../removeHost.js';

export default ({
    program,
    screenStream,
    onAbort,
    user
}) => program
    .command('remove-host')
    .description(getString('removeHost.title'))
    .argument('<name>', getString('setHost.name'))
    .action(async name => {
        await removeHost({
            screenStream,
            onAbort,
            user,
            name
        });
    });