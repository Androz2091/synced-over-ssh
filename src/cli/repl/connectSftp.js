import {
    PassThrough
} from 'node:stream';

import chalk from 'chalk';

import {
    chainedSession
} from '../../../lib/ssh.js';
import getHostChain from '../../../lib/getHostChain.js';

import {
    getString,
    getAnyString
} from '../../../strings.js';

export default ({
    setActiveStream,
    program,
    screenStream,
    user
}) => program
    .command('connect-sftp')
    .argument('<name1>')
    .argument('<name2>')
    .action(async (name1, name2) => {
        screenStream.stdin.write(`${getString('connectHost.connecting')}\r\n`);
        const
            hosts = user.getHosts(),
            host1 = hosts.find(host => name1 === host.name),
            host2 = hosts.find(host => name2 === host.name);
        try {
            const
                {
                    stream: serverStream,
                    end: endClient
                } = await chainedSession({
                    hostsChain: getHostChain(hosts, host1),
                    command: `mc ~ sftp://${host2.username}:${host2.password.replaceAll(/(.)/g, '\\$1')}@${host2.address}:${host2.port}/home/${host2.username}`
                }),
                serverStdin = new PassThrough(),
                serverStdout = new PassThrough();
            serverStream.on('end', () => {
                endClient();
                setActiveStream(screenStream);
            });
            serverStream.on('data', data => serverStdin.write(data));
            serverStdout.on('data', data => serverStream.write(data));
            setActiveStream({
                stdin: serverStdin,
                stdout: serverStdout,
                setSize: ({ rows, columns }) => serverStream.setWindow(rows, columns)
            }, false);
        }
        catch(error){
            setActiveStream(screenStream, false);
            screenStream.stdin.write(`${chalk.red(getAnyString(
                `setHost.${getString(`setHost.rawError.${error.level}`)}`,
                ['COMMAND_FAILED', { error }]
            ))}\n`);
        }
    });