import { PassThrough } from 'node:stream';
import repl from 'node:repl';

import { Command } from 'commander';
import chalk from 'chalk';
import { parse } from 'shell-quote';

import listHosts from './listHosts.js';
import addHost from './addHost.js';
import editHost from './editHost.js';
import removeHost from './removeHost.js';
import connectHost from './connectHost.js';
import connectSftp from './connectSftp.js';
import hosts from './hosts.js';
import disableTotp from './disableTotp.js';
import enableTotp from './enableTotp.js';
import setUsername from './setUsername.js';
import setPassword from './setPassword.js';
import quit from './quit.js';
import unregister from './unregister.js';
import { getString } from '../../../strings.js';

export default async ({
    setActiveStream,
    screenStream,
    end,
    user
}) => {
    let isIdle = true;
    const
        replStdin = new PassThrough(),
        replStdout = new PassThrough();
    screenStream.stdout.on('data', data => {
        if(isIdle)
            replStdin.write(data);
    });
    replStdout.on('data', data => {
        if(isIdle)
            screenStream.stdin.write(data);
    });
    const createProgram = ({
        onOutput,
        onAbort
    }) => {
        const program = new Command();
        program.exitOverride();
        program.configureOutput({
            writeOut: async output => onOutput(output),
            writeErr: async output => onOutput(output)
        });
        listHosts({
            program,
            screenStream,
            user
        });
        addHost({
            program,
            screenStream,
            onAbort,
            user
        });
        editHost({
            program,
            screenStream,
            onAbort,
            user
        });
        removeHost({
            program,
            screenStream,
            onAbort,
            user
        });
        connectHost({
            setActiveStream,
            program,
            screenStream,
            user
        });
        connectSftp({
            setActiveStream,
            program,
            screenStream,
            user
        });
        hosts({
            setActiveStream,
            program,
            screenStream,
            onAbort,
            user
        });
        disableTotp({
            program,
            screenStream,
            onAbort,
            user
        });
        enableTotp({
            program,
            screenStream,
            onAbort,
            user
        });
        setUsername({
            program,
            screenStream,
            onAbort,
            user
        });
        setPassword({
            program,
            screenStream,
            onAbort,
            user
        });
        quit({
            program,
            end
        });
        unregister({
            program,
            screenStream,
            onAbort,
            user,
            end
        });
        return program;
    };
    try {
        await createProgram({
            onOutput: data => screenStream.stdin.write(data.replaceAll('\n', '\r\n'))
        }).parseAsync(
            [user.getHosts().length ? 'l' : 'help'],
            { from: 'user' }
        );
    }
    catch {}
    repl.start({
        input: replStdin,
        output: replStdout,
        terminal: true,
        eval: async (
            line,
            _,
            __,
            callback
        ) => {
            if(line === '\n') return callback();
            isIdle = false;
            let output;
            const
                onOutput = _output => output = _output.replaceAll('\n', '\r\n'),
                onEnd = () => {
                    isIdle = true;
                    callback();
                },
                onAbort = () => {
                    screenStream.stdin.write(`\r\n${chalk.red(getString('COMMAND_ABORTED'))}\r\n`);
                    onEnd();
                },
                program = createProgram({
                    onOutput,
                    onAbort
                });
            try {
                await program.parseAsync(
                    parse(line),
                    {
                        from: 'user'
                    }
                );
            }
            catch(error){
                if(output){
                    screenStream.stdin.write(
                        [
                            'commander.help',
                            'commander.helpDisplayed'
                        ].includes(error.code)
                            ? output
                            : chalk.red(output)
                    );
                }
                else
                    screenStream.stdin.write(`${chalk.red(getString('COMMAND_FAILED', { message: error.message }))}\n`);
            }
            onEnd();
        }
    }).on('exit', end);
};