import chalk from 'chalk';
import inquirerInput from '@inquirer/input';

import { getString } from '../../../strings.js';
import createInquirerContext from '../../../lib/createInquirerContext.js';

export default ({
    program,
    screenStream,
    onAbort,
    user
}) => program
    .command('disable-totp')
    .description(getString('home.disableTotp'))
    .argument('[totpCode]', getString('totpCode'))
    .action(async totpCode => {
        if(!user.isTotpEnabled())
            return screenStream.stdin.write(`${chalk.red(getString('disableTotp.notEnabledError'))}\n`);
        if(totpCode){
            try {
                user.disableTotp(totpCode);
                screenStream.stdin.write(`${chalk.green(getString('disableTotp.success'))}\n`);
            }
            catch {
                screenStream.stdin.write(`${chalk.red(getString('invalidTotpCode'))}\n`);
            }
        }
        else {
            await inquirerInput(
                {
                    message: getString('totpCode'),
                    validate: code => {
                        try {
                            return user.disableTotp(code);
                        }
                        catch {
                            return getString('invalidTotpCode');
                        }
                    }
                },
                createInquirerContext(screenStream, onAbort)
            );
            screenStream.stdin.write(`${chalk.green(getString('disableTotp.success'))}\n`);
        }
    });