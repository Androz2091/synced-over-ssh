import {
    login as loginAccount,
    preregister as preregisterAccount,
    register as registerAccount
} from '@sshception/account';
import AsciiTable from 'ascii-table';
import chalk from 'chalk';
import inquirerConfirm from '@inquirer/confirm';
import inquirerPassword from '@inquirer/password';
import inquirerInput from '@inquirer/input';

import createPassthroughStream from '../../lib/createPassthroughStream.js';
import repl from './repl/main.js';
import { getString } from '../../strings.js';
import packageJson from '../../package.json' assert { type: 'json' };
import createInquirerContext from '../../lib/createInquirerContext.js';
import enableTotp from './enableTotp.js';

export default async ({
    streamManager: {
        setActiveStream
    },
    username,
    password,
    end
}) => {
    const screenStream = createPassthroughStream();
    setActiveStream(screenStream);
    let
        user,
        loginError,
        preregisterError;
    try {
        user = await loginAccount(username, password);
    }
    catch(error){
        loginError = error;
    }
    if(user){
        if(user.isTotpLocked()){
            await inquirerInput(
                {
                    message: getString('totpCode'),
                    validate: code => user.unlockTotp(code) || getString('invalidTotpCode')
                },
                createInquirerContext(screenStream, end)
            );
        }
        return repl({
            setActiveStream,
            screenStream,
            end,
            user
        });
    }
    if(loginError){
        switch(loginError.message){
            case 'WRONG_USERNAME': {
                try {
                    await preregisterAccount(username, password);
                }
                catch(error){
                    preregisterError = error;
                }
                break;
            }
            default: {
                await new Promise(resolve => setTimeout(resolve, 500));
                screenStream.stdin.write(`${chalk.red(getString(loginError.message))}\r\n`);
                return end();
            }
        }
    }
    if(preregisterError){
        screenStream.stdin.write(`${chalk.red(getString(preregisterError.message))}\r\n`);
        return end();
    }
    screenStream.stdin.write(
        new AsciiTable()
            .addRowMatrix(getString('disclaimer.title', { returnObjects: true, packageJson }))
            .toString()
            .replaceAll('\n', '\r\n')
        +
        '\r\n'
    );
    if(!await inquirerConfirm(
        {
            message: getString('proceed'),
            default: false
        },
        createInquirerContext(screenStream, end)
    )) return end();
    screenStream.stdin.write(`${getString('register.title', { username })}\r\n`);
    await inquirerPassword(
        {
            message: getString('password'),
            mask: '*',
            validate: passwordConfirmation => password === passwordConfirmation || getString('register.passwordMismatch')
        },
        createInquirerContext(screenStream, end)
    );
    user = await registerAccount(username, password);
    if(await inquirerConfirm(
        {
            message: getString('home.enableTotp'),
            default: false
        },
        createInquirerContext(screenStream, end)
    )){
        await enableTotp({
            screenStream,
            onAbort: end,
            user
        });
    }
    return repl({
        screenStream,
        end,
        user
    });
};