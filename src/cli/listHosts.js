import AsciiTable from 'ascii-table';

import {
    getString
} from '../../strings.js';

export default ({
    screenStream,
    user
}) => {
    screenStream.stdin.write(
        new AsciiTable()
            .setHeading('Host', 'URI')
            .addRowMatrix(user.getHosts().map(host => [
                host.chain
                    ? getString(
                        'home.chain',
                        {
                            host,
                            via: host.chain
                                .split(',')
                                .join(getString('home.chainSeparator'))
                        }
                    )
                    : host.name,
                getString('home.hostUri', { host })
            ]))
            .toString()
            .replaceAll('\n', '\r\n')
        +
        '\r\n'
    );
};