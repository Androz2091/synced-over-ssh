import chalk from 'chalk';

import {
    getString
} from '../../strings.js';

import setHost from './setHost.js';
import testHost from './testHost.js';

export default async ({
    screenStream,
    onAbort,
    user
}) => {
    const
        hosts = user.getHosts(),
        host = await setHost({
            screenStream,
            onAbort,
            hosts
        });
    const isSuccess = await testHost({
        screenStream,
        onAbort,
        hosts,
        host
    });
    if(isSuccess){
        user.addHost(host);
        screenStream.stdin.write(`${chalk.green(getString('setHost.successAdd'))}\n`);
    }
};