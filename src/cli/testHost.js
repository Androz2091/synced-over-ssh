import inquirerConfirm from '@inquirer/confirm';
import chalk from 'chalk';

import createInquirerContext from '../../lib/createInquirerContext.js';
import {
    chainedSession
} from '../../lib/ssh.js';
import {
    getString,
    getAnyString
} from '../../strings.js';

export default async ({
    screenStream,
    onAbort,
    hosts,
    host
}) => {
    let testError;
    if(await inquirerConfirm(
        {
            message: getString('setHost.test'),
            default: false
        },
        createInquirerContext(screenStream, onAbort)
    )){
        const hostsChain = [
            ...host.chain
                ? host.chain
                    .split(',')
                    .map(name => hosts.find(host => name === host.name))
                : [],
            host
        ];
        if(hostsChain.includes(undefined)){
            screenStream.stdin.write(`${chalk.red(getString('setHost.INVALID_CHAIN'))}\n`);
            return false;
        }
        try {
            await chainedSession({
                hostsChain,
                isTest: true
            });
        }
        catch(error){
            testError = error;
        }
    }
    if(testError){
        screenStream.stdin.write(`${chalk.red(getAnyString(
            `setHost.${getString(`setHost.rawError.${testError.level}`)}`,
            ['COMMAND_FAILED', { testError }]
        ))}\n`);
    }
    return !testError;
};