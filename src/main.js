export default async ({
    session
}) => {
    let
        rows,
        columns,
        activeStream;
    const
        env = {};
    session.on('env', (_, __, { key, val }) => env[key] = val);
    const
        clientStream = await new Promise(resolve => session.on('pty', (accept, reject, info) => {
            ({
                rows,
                cols: columns
            } = info);
            session.on('shell', accept => resolve(accept()));
            accept();
        })),
        applyStreamSize = () => {
            clientStream.rows = rows;
            clientStream.columns = columns;
            clientStream.emit('resize');
            activeStream.setSize({ rows, columns });
        },
        setActiveStream = (_stream, isClear = true) => {
            if(activeStream){
                if(isClear)
                    activeStream.stdin.write(`${' '.repeat(columns)}\r\n`.repeat(rows));
                activeStream.stdin.unpipe(clientStream.stdin);
                clientStream.stdout.unpipe(activeStream.stdout);
            }
            _stream.stdin.pipe(clientStream.stdin);
            clientStream.stdout.pipe(_stream.stdout);
            activeStream = _stream;
            applyStreamSize();
        };
    session.on('window-change', (_, __, size) => {
        ({
            rows,
            cols: columns
        } = size);
        applyStreamSize();
    });
    return {
        getSize: () => ({
            rows,
            columns
        }),
        env,
        setActiveStream
    };
};