export default (hosts, host) => [
    ...(
        host.chain
            ? host.chain
                .split(',')
                .map(name => hosts.find(host => host.name === name))
            : []
    ),
    host
];