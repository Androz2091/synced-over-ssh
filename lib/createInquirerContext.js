import {
    PassThrough
} from 'node:stream';

export default (
    stream,
    onAbort,
    initialInput
) => {
    const
        stdin = new PassThrough(),
        stdout = new PassThrough(),
        fromInquirerToClient = data => stream.stdin.write(Buffer.from(data.toString().replaceAll('\n', '\r\n'), 'utf8')),
        fromClientToInquirer = data => {
            switch(data.toString('hex')){
                case '03': {
                    stdout.removeListener('data', fromInquirerToClient);
                    stream.stdout.removeListener('data', fromClientToInquirer);
                    stdin.destroy();
                    stdout.destroy();
                    onAbort();
                    break;
                }
                default: {
                    stdin.write(data);
                    break;
                }
            }
        };
    stdout.on('end', () => {
        stdout.removeListener('data', fromInquirerToClient);
        stream.stdout.removeListener('data', fromClientToInquirer);
        stdin.destroy();
    });
    stdout.on('data', fromInquirerToClient);
    stream.stdout.on('data', fromClientToInquirer);
    if(initialInput)
        setTimeout(() => stdin.write(initialInput.toString()), 0);
    return {
        input: stdin,
        output: stdout
    };
};