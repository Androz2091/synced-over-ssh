export const
    /**
     * SSH server listening port
     * @type {number}
     */
    sshPort = undefined;